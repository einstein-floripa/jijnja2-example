# Olá, Einsteiniano!

Faça bom proveito deste exemplo =)

Feito para rodar em Linux. Provável que funcione em Windows apenas mudando `/` para `\` nos caminhos de arquivos.

## Como rodar

Primeiro, garanta que tem [Python 3.X](https://www.python.org/) instalado.

Após, garanta que tem [`pip`](https://pip.pypa.io/en/stable/installing/) instalado.

```
pip install pipenv --user
pipenv install
pipenv run python run.py
```

O arquivo de saída estará na pasta raíz do exemplo =)