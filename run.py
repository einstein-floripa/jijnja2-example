from jinja2 import Environment
from weasyprint import HTML, CSS
from weasyprint.fonts import FontConfiguration
import os

context = {
    'document_title': 'Cartinha de boas vindas',
    'header_text': 'Bem vindo a Inteligência, Einsteiniano!',
    'message':'''Olá, novo Einsteiniano(a)!
    <br><br>
    Primeiramente, parabéns! Você passou pelo processo seletivo, e
    foi escolhido(a) por nossa comissão como um dos novos integrantes
    do nosso time! Estamos muito felizes por termos você! Seja bem 
    vindo a Inteligência, e nunca se esqueça:
    Seja sempre a pessoa mais inteligente da sala..
    <br>
    <br>
    Esse é um exemplo bem simples, mas serve como boa base para você
    criar seus próprios templates e entender melhor como Jinja funciona =) 
    ''',
    'list_title': 'Algumas skills que você desenvolverá',
    'list': [
        'Visão estratégica', 
        'Habilidades de programação',
        'Análise de dados',
        'Liderança',
    ],
}

# Apenas algumas configurações básicas
output_filename = 'output.pdf'
template_file = 'templates/feedback_template.html'
css_files = ['templates/feedback_template.css']

# Font config serve para na renderização, as fontes configuradas no CSS
# serem habilitadas
font_config = FontConfiguration()

# Carregando o template de um arquivo, usando o read da stream de arquivo
# Atençãozinha para a keyword `with` que aqui serve para garantir que o arquivo
# será fechado após o bloco de código
with open(template_file) as f:
    template = Environment().from_string(f.read())

# Os arquivos CSS podem ser incluidos dessa forma.
# Apenas como uma lista de objetos da classe CSS
css = list()
for file in css_files:
    css.append(CSS(filename=file, font_config=font_config))

rendered = template.render(context)
HTML(string=rendered,
     base_url=os.getcwd() + '/templates').write_pdf(output_filename,
                                                    stylesheets=css,
                                                    font_config=font_config)
